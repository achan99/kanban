import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    pageBody: 
    {
        height: '100vh', 
        width: '100vw',
        background: '#AAAAAA',
        display: 'flex',
        justifyContent: 'space-around',
    },
    root: {
        width: '90%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    taskDesign: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'space-around',
        width: '100%'
    }
});

class KanBan extends Component {
    constructor(props){
        super(props);
        this.state = {
        boardState: this.props.boardStates
        };
    }
  
  clickLeft(moved_task, currState){

    var newBoardState = this.state.boardState;
    var states = newBoardState.map((state)=>{
      return state.stateName
    })

    var newState = states[states.indexOf(currState) - 1];

    newBoardState.forEach((card)=>{
      var task_list = card.tasks.map((task)=>{
            return task.name
        })
        if (card.stateName === newState){
            card.tasks.push(moved_task)
        }
        else if (card.stateName === currState){
            card.tasks.splice( task_list.indexOf(moved_task.name), 1 );
        }
    })
    this.setState({boardState: newBoardState})
  }

  clickRight(moved_task, currState){

    var newBoardState = this.state.boardState;
    var states = newBoardState.map((state)=>{
        return state.stateName
    })

    var newState = states[states.indexOf(currState) + 1];

    newBoardState.forEach((card)=>{
        var task_list = card.tasks.map((task)=>{
            return task.name
        })
        if (card.stateName === newState){
            card.tasks.push(moved_task)
        }
        else if (card.stateName === currState){
            card.tasks.splice( task_list.indexOf(moved_task.name), 1 );
        }
    })
    this.setState({boardState: newBoardState})
  }

  
  render() {
    const { classes } = this.props;

    return (
      <div className = {classes.pageBody}>
        {this.state.boardState.map((card) =>
          <Paper key={card.stateName} className = {classes.root}>
              <h1>{card.stateName}</h1>
                {card.tasks.map((task) => 
                    <MenuItem key={task.name}>
                    <div className={classes.taskDesign}>
                      {card.id === 0 ?  
                        <Button disabled variant="contained" color="primary" className={classes.button}>L</Button>
                        :
                        <Button onClick={()=>this.clickLeft(task, card.stateName)} variant="contained" color="primary" className={classes.button}>L</Button>
                        }
                         {task.name}
                         {card.id === this.state.boardState.length - 1 ?  
                        <Button disabled variant="contained" color="primary" className={classes.button}>R</Button>
                        :
                        <Button onClick={()=>this.clickRight(task, card.stateName)} variant="contained" color="primary" className={classes.button}>R</Button>
                        }
                      </div>
                  </MenuItem>
                  )}
          </Paper>
        )}
      </div>
    );
  }
}
KanBan.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(KanBan);
