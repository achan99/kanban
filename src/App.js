import React, { Component } from 'react';
import KanBan from './KanBan.js'
class App extends Component {
  render() {
    return (
      <div>
          <KanBan boardStates={[{stateName: 'Design', id:0, tasks:[{name: 'task 1'}, {name: 'task 2'}]}, {stateName: 'Prototype', id:1, tasks:[{name: 'task 3'}, {name: 'task 4'}]}, {stateName: 'Build', id:2, tasks:[{name: 'task 5'}]}]}></KanBan>

      </div>
    );
  }
}

export default App;
